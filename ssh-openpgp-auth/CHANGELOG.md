<!--
# SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
# SPDX-FileCopyrightText: 2024 Wiktor Kwapisiewicz <wiktor@metacode.biz>
# SPDX-License-Identifier: CC0-1.0
-->
# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1](https://codeberg.org/wiktor/ssh-openpgp-auth/compare/ssh-openpgp-auth-v0.2.0...ssh-openpgp-auth-v0.2.1) - 2024-02-10

### Other
- update Cargo.lock dependencies
