<!--
SPDX-FileCopyrightText: 2024- Wiktor Kwapisiewicz <wiktor@metacode.biz>
SPDX-License-Identifier: MIT OR Apache-2.0
-->

# SSH OpenPGP Authenticator

This project aims to improve the security of SSH connections by providing a way to verify host keys using OpenPGP certificates.

Why OpenPGP certificates? OpenPGP already has a trust model built-in, and the tools provided here can leverage existing trust paths that the user may already have (e.g. by using [OpenPGP CA](https://openpgp-ca.org/)).

Our solution consists of two components:

  - `sshd-openpgp-auth`: Host administrator-side tool that manages the "host certificate",
  - `ssh-openpgp-auth`: User-side tool that fetches host certificates, verifies trust chains, and configures the OpenSSH client to automatically trust all verified host keys.

## Technical details

When using the tools provided by this project, an OpenPGP certificate represents the identity of an SSH host.

Users may trust a host's OpenPGP certificate either explicitly or transitively (e.g., as part of an OpenPGP CA setup). All subkeys of the host's OpenPGP certificate are *Authentication keys*, one for each SSH host key. When the user's system has verified the host's OpenPGP certificate, the user's software transparently recognizes and trusts all SSH keys of that host, even when the remote host cycle some or all of its SSH keys. This works because the server operator links each of the host's SSH public keys to the host's OpenPGP certificate. The OpenPGP certificate acts as a Public Key Infrastructure (PKI) mechanism, which binds the public SSH host keys to a stable identity.

In setups where OpenPGP trust chains are already in use, the additional concern of authenticating SSH host identities is a straightforward and seamless extension, with these tools.

## Funding

This project is funded through [NGI Assure](https://nlnet.nl/assure), a fund established by [NLnet](https://nlnet.nl) with financial support from the European Commission's [Next Generation Internet](https://ngi.eu) program. Learn more at the [NLnet project page](https://nlnet.nl/OpenPGP-OpenSSH).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGIAssure_tag.svg" alt="NGI Assure Logo" width="20%" />](https://nlnet.nl/assure)

## License

This project is licensed under either of:

  - [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0),
  - [MIT license](https://opensource.org/licenses/MIT).

at your option.

### Contribution

Unless you explicitly state otherwise, any contribution intentionally
submitted for inclusion in this crate by you, as defined in the
Apache-2.0 license, shall be dual licensed as above, without any
additional terms or conditions.
