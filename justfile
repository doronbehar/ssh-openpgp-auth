#!/usr/bin/env -S just --working-directory . --justfile
# SPDX-FileCopyrightText: 2024 David Runge <dave@sleepmap.de>
# SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
# SPDX-License-Identifier: CC0-1.0
#
# Load project-specific properties from the `.env` file

set dotenv-load := true

# Runs all checks for currently checked files

# Since this is a first recipe it's being run by default.
check-files: spelling formatting lints dependencies licenses tests

# Faster checks need to be executed first for better UX.  For example
# codespell is very fast. cargo fmt does not need to download crates etc.

# Installs all tools required for development
install: install-packages install-tools

# Install development packages using pacman
install-packages:
    # Packages that are needed by this justfile are listed directly
    # Any extra packages are set in the `.env` file
    pacman -Syu --needed --noconfirm rustup cocogitto codespell reuse cargo-deny tangler rust-script $PACMAN_PACKAGES

# Installs any user tools required to run development tooling
install-tools:
    rustup default stable
    rustup component add clippy
    rustup toolchain install nightly
    rustup component add --toolchain nightly rustfmt

# Runs all tasks intended for the CI environment
ci: check-files e2e

# Checks common spelling mistakes
spelling:
    codespell

# Checks source code formatting
formatting:
    just --unstable --fmt --check
    # We're using nightly to properly group imports, see rustfmt.toml
    cargo +nightly fmt -- --check

# Lints the source code
lints:
    cargo clippy --all -- -D warnings

# Checks for issues with dependencies
dependencies:
    cargo deny check -D warnings -A duplicate -A accepted

# Checks licensing status
licenses:
    reuse lint

# Runs all unit tests
tests:
    cargo test --all

# Runs all end-to-end tests
e2e:
    #!/usr/bin/env bash
    set -euo pipefail
    for dir in *; do
        if [[ -d "$dir" && -f "$dir/Cargo.toml" && -f "$dir/README.md" ]]; then
            just test-readme "$dir"
        fi
    done

# Runs per project end-to-end tests found in a project README
test-readme project:
    #!/usr/bin/env bash
    if [[ -f "$EXE_DIR/{{ project }}" ]]; then
        printf "Precompiled version of %s found.\n" "{{ project }}"
        PATH="$EXE_DIR:$PATH"
    else
        printf "No precompiled version of %s found, installing...\n" "{{ project }}"
        cargo install --path {{ project }}
        if [[ -n "$CARGO_HOME" ]]; then
            PATH="$CARGO_HOME/bin:$PATH"
        else
            PATH="$HOME/.cargo/bin:$PATH"
        fi
    fi
    printf "PATH=%s\n" "$PATH"
    cd {{ project }} && tangler sh < README.md | bash -euxo pipefail -

# Adds git hooks (pre-commit, pre-push)
add-hooks:
    #!/usr/bin/env bash
    echo just check-files > .git/hooks/pre-commit
    chmod +x .git/hooks/pre-commit

    echo just check-commits > .git/hooks/pre-push
    chmod +x .git/hooks/pre-push

# Checks for commit messages
check-commits REFS='main..':
    #!/usr/bin/env bash
    set -euo pipefail
    for commit in $(git rev-list "{{ REFS }}"); do
      MSG="$(git show -s --format=%B "$commit")"
      CODESPELL_RC="$(mktemp)"
      git show "$commit:.codespellrc" > "$CODESPELL_RC"
      if ! grep -q "Signed-off-by: " <<< "$MSG"; then
        printf "Commit %s lacks \"Signed-off-by\" line.\n" "$commit"
        printf "%s\n" \
            "  Please use:" \
            "    git rebase --signoff main && git push --force-with-lease" \
            "  See https://developercertificate.org/ for more details."
        exit 1;
      elif ! codespell --config "$CODESPELL_RC" - <<< "$MSG"; then
        printf "The spelling in commit %s needs improvement.\n" "$commit"
        exit 1;
      elif ! cog verify "$MSG"; then
        exit 1;
      else
        printf "Commit %s formatting and spelling is good.\n" "$commit"
      fi
    done

# Create a CA cert and sign a TLS key with it
create-ci-tls-ca:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly org=localhost-ca
    readonly domain="${DOMAIN:-example.com}"
    readonly output_dir=${CERT_DIR:-.}

    mkdir "$output_dir"
    openssl genpkey -algorithm RSA -out "$output_dir/ca.key"
    openssl req -x509 -key "$output_dir/ca.key" -out "$output_dir/ca.crt" -subj "/CN=$org/O=$org"

    openssl genpkey -algorithm RSA -out "$output_dir/$domain".key
    openssl req -new -key "$output_dir/$domain.key" -out "$output_dir/$domain.csr" -subj "/CN=$domain/O=$org"

    openssl x509 -req -in "$output_dir/$domain.csr" -days 365 -out "$output_dir/$domain.crt" -CA "$output_dir/ca.crt" -CAkey "$output_dir/ca.key" -CAcreateserial -extfile <(cat <<END
    basicConstraints = CA:FALSE
    subjectKeyIdentifier = hash
    authorityKeyIdentifier = keyid,issuer
    subjectAltName = DNS:$domain
    END
    )

# Trust a local Certificate Authority (CA). This target assumes elevated privileges to alter the system's trust store.
trust-ci-tls-ca:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly output_dir="${CERT_DIR:-.}"
    trust anchor "$output_dir/ca.crt"

# Prepare a test host. This target should be used in a container, as it is destructive to the host and assumes no running services and being run as root. It prepares the SSH configuration for root, so that ssh-openpgp-auth can be used as KnownHostsCommand and successful logins print a message and log the user out automatically.
prepare-ci-test-host:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly soa_auth_options="${SOA_AUTH_OPTIONS:---verbose}"

    # allow providing just the command name if `EXE_DIR` is an empty string
    if [[ -n "$EXE_DIR" ]]; then
        exe_dir="${EXE_DIR}/"
    else
        exe_dir=""
    fi

    printf "127.0.0.1 %s\n" "$fqdn" >> /etc/hosts

    printf "#!/bin/bash\nprintf 'Logged in over ssh!\nLogging out...\n'\nexit 0\n" > /usr/local/bin/rootlogin
    chmod +x /usr/local/bin/rootlogin
    printf "Match user root\n  ForceCommand /usr/local/bin/rootlogin\n" > /etc/ssh/sshd_config.d/10-rootlogin.conf
    ssh-keygen -A
    /usr/bin/sshd

    mkdir -p /root/.ssh
    ssh-keygen -q -f /root/.ssh/id_ed25519 -N ""
    cat /root/.ssh/*.pub > /root/.ssh/authorized_keys
    cat /root/.ssh/authorized_keys
    printf "Host example.com\n  KnownHostsCommand %sssh-openpgp-auth authenticate %s %%H\n" "$exe_dir" "$soa_auth_options" > /root/.ssh/config
    cat /root/.ssh/config

# Use miniserve to expose a Web Key Directory (WKD) locally over TLS. This assumes being able to use port 443 (which mostly requires root).
host-ci-wkd-dir:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly cert_dir="${CERT_DIR:-.}"
    readonly fqdn="${DOMAIN:-example.com}"
    readonly wkd_dir="${WKD_DIR:-wkd}"
    if [[ ! -d "$wkd_dir" ]]; then
        mkdir -p "$wkd_dir"
    fi
    miniserve --hidden --tls-cert "$cert_dir/$fqdn.crt" --tls-key "$cert_dir/$fqdn.key" --interfaces 127.0.0.1 --port 443 "$wkd_dir" &

# Use ssh to connect to a domain
connect-ci-ssh:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly soa_auth_options="${SOA_AUTH_OPTIONS:---verbose}"

    if ! ssh "$fqdn" -vv ; then
        if [[ -n "$EXE_DIR" ]]; then
            PATH="$EXE_DIR:$PATH"
        fi

        # run ssh-openpgp-auth authenticate again with the same options in case we are failing to show its output
        read -ra auth_options <<<"$soa_auth_options"
        set +e
        ssh-openpgp-auth authenticate "${auth_options[@]}" "$fqdn"
        set -e
        exit 1
    fi

# Setup a host in such a way, that connecting via SSH is a success
setup-ci-local-trust-anchor:
    #!/usr/bin/env bash
    set -euo pipefail
    readonly fqdn="${DOMAIN:-example.com}"
    readonly wkd_dir="${WKD_DIR:-wkd}"
    readonly cert_dir="${CERT_DIR:-.}"

    if [[ -n "$EXE_DIR" ]]; then
        PATH="$EXE_DIR:$PATH"
    fi

    # prepare TSK to manage SSH host keys as authentication subkeys
    sshd-openpgp-auth init "$fqdn"
    sshd-openpgp-auth add
    sshd-openpgp-auth list

    # create a new TSK to serve as local trust anchor
    sq key generate --userid 'Foobar McFooface <foobar@mcfooface.com>' --output "$cert_dir/local_trust_anchor.key.pgp"
    # import certificate of local trust anchor to cert store
    sq key extract-cert "$cert_dir/local_trust_anchor.key.pgp" | sq cert import
    # set the newly added certificate up as trusted introducer (CA)
    trust_anchor_fingerprint="$(sq inspect "$cert_dir/local_trust_anchor.key.pgp" 2>&1 | sed -ne 's/.*Fingerprint: \(.*\)/\1/p')"
    printf "Local trust anchor fingerprint: %s\n" "$trust_anchor_fingerprint"
    sq pki link add --all --ca "$fqdn" "$trust_anchor_fingerprint"

    for key in /var/lib/sshd-openpgp-auth/*.asc; do
        cert="$(basename "$key").pgp"
        fingerprint="${cert/.*/}"

        printf "certificate %s\n" "$cert"
        printf "fingerprint %s\n" "$fingerprint"

        # extract certificate from host TSK
        sq key extract-cert --output "$cert_dir/$cert" "$key"
        sq inspect "$cert_dir/$cert"

        # use local trust anchor to certify the host's certificate
        sq pki certify --output "$cert_dir/$cert.asc" "$cert_dir/local_trust_anchor.key.pgp" "$cert_dir/$cert" "<ssh-openpgp-auth@$fqdn>"
        sq inspect --certifications "$cert_dir/$cert.asc"

        # export the updated host certificate (including certification by local trust anchor)
        sshd-openpgp-auth export --wkd-type direct --openpgp-dir "$cert_dir" --output-dir "$wkd_dir" "$fqdn"
        tree -a "$wkd_dir"
        sshd-openpgp-auth export --openpgp-dir "$cert_dir" --output-dir "$wkd_dir" "$fqdn"
        tree -a "$wkd_dir"
    done

# Run a full integration test against a setup validating via WKD and validating the PKI based on a local trust anchor acting as CA for the domain
test-ci-local-trust-anchor: create-ci-tls-ca trust-ci-tls-ca prepare-ci-test-host host-ci-wkd-dir setup-ci-local-trust-anchor connect-ci-ssh

# Fixes common issues. Files need to be git add'ed
fix:
    #!/usr/bin/env bash
    if ! git diff-files --quiet ; then
        echo "Working tree has changes. Please stage them: git add ."
        exit 1
    fi

    codespell --write-changes
    just --unstable --fmt
    cargo clippy --fix --allow-staged

    # fmt must be last as clippy's changes may break formatting
    cargo +nightly fmt

render-script := '''
    //! ```cargo
    //! [dependencies]
    //! pkg = { path = "PKG", package = "PKG" }
    //! clap_allgen = "0.1.0"
    //! ```

    fn main() -> Result<(), Box<dyn std::error::Error>> {
        clap_allgen::render_KIND::<pkg::Commands>(
            &std::env::args().collect::<Vec<_>>()[1],
        )?;
        Ok(())
    }
'''

# Render `manpages` or `shell_completions` (`kind`) of a given package (`pkg`) to directory given by `output`.
generate kind pkg output:
    sed 's/PKG/{{ pkg }}/g;s/KIND/{{ kind }}/g' > .script.rs <<< '{{ render-script }}'
    rust-script .script.rs '{{ output }}'
    rm --force .script.rs
