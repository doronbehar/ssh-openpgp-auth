// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-FileCopyrightText: 2023 Wiktor Kwapisiewicz <wiktor@metacode.biz>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use std::io::stdin;
use std::path::PathBuf;
use std::time::Duration;

use clap::Parser;
use sshd_openpgp_auth::add_dns_proof;
use sshd_openpgp_auth::attach_subkeys_to_cert;
use sshd_openpgp_auth::create_openpgp_subkey_from_ssh_public_key;
use sshd_openpgp_auth::create_openpgp_subkey_from_ssh_public_key_file;
use sshd_openpgp_auth::create_trust_anchor;
use sshd_openpgp_auth::export_certs_to_wkd;
use sshd_openpgp_auth::extend_expiry_of_cert;
use sshd_openpgp_auth::get_public_ssh_host_keys;
use sshd_openpgp_auth::get_single_cert_from_dir;
use sshd_openpgp_auth::parse_known_hosts;
use sshd_openpgp_auth::read_all_certs;
use sshd_openpgp_auth::revoke_subkey_of_cert;
use sshd_openpgp_auth::show_tsks_in_dir;
use sshd_openpgp_auth::write_tsk;
use sshd_openpgp_auth::write_tsk_to_stdout;
use sshd_openpgp_auth::Commands;
use sshd_openpgp_auth::DnsCommand;
use sshd_openpgp_auth::ProofCommand;
use sshd_openpgp_auth::SECONDS_IN_A_DAY;
use sshd_openpgp_auth::WKD_OUTPUT_DIR;
use sshd_openpgp_auth::WKD_TYPE;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let command = Commands::parse();
    match command {
        Commands::Add(cmd) => {
            let cert = get_single_cert_from_dir(cmd.openpgp_dir.as_deref(), cmd.fingerprint)?;
            let openpgp_subkeys = if cmd.known_hosts {
                let stdin = stdin();
                let input = stdin.lines().fold(String::new(), |s, line| {
                    s + &line.unwrap_or_default() + "\n"
                });
                parse_known_hosts(&input)?
                    .iter()
                    .filter_map(|ssh_public_key| {
                        create_openpgp_subkey_from_ssh_public_key(
                            ssh_public_key.to_owned(),
                            cmd.time.map(Into::into),
                        )
                        .ok()
                    })
                    .collect()
            } else {
                let ssh_host_key_files = get_public_ssh_host_keys(cmd.ssh_dir.as_deref())?;
                ssh_host_key_files
                    .iter()
                    .filter_map(|ssh_host_key_file| {
                        create_openpgp_subkey_from_ssh_public_key_file(
                            ssh_host_key_file,
                            cmd.time.map(Into::into),
                        )
                        .ok()
                    })
                    .collect()
            };
            let cert = attach_subkeys_to_cert(cert, openpgp_subkeys)?;

            if cmd.stdout {
                write_tsk_to_stdout(&cert)?
            } else {
                write_tsk(&cert, cmd.openpgp_dir.as_deref())?
            }
        }
        Commands::Export(cmd) => export_certs_to_wkd(
            read_all_certs(cmd.openpgp_dir.as_deref())?,
            cmd.hostname,
            cmd.wkd_type.unwrap_or(WKD_TYPE),
            &cmd.output_dir.unwrap_or(PathBuf::from(WKD_OUTPUT_DIR)),
            cmd.time.map(Into::into),
        )?,
        Commands::Extend(cmd) => {
            let cert = get_single_cert_from_dir(cmd.openpgp_dir.as_deref(), cmd.fingerprint)?;
            let cert = extend_expiry_of_cert(
                cert,
                cmd.threshold
                    .map(|threshold| Duration::new(threshold * SECONDS_IN_A_DAY, 0)),
                cmd.expiry
                    .map(|expiry| Duration::new(expiry * SECONDS_IN_A_DAY, 0)),
                cmd.time.map(Into::into),
            )?;

            if cmd.stdout {
                write_tsk_to_stdout(&cert)?
            } else {
                write_tsk(&cert, cmd.openpgp_dir.as_deref())?
            }
        }
        Commands::Init(cmd) => {
            let cert = create_trust_anchor(
                &cmd.host,
                cmd.time.map(Into::into),
                cmd.expiry
                    .map(|expiry| Duration::new(expiry * SECONDS_IN_A_DAY, 0)),
            )?;

            if cmd.stdout {
                write_tsk_to_stdout(&cert)?
            } else {
                write_tsk(&cert, cmd.openpgp_dir.as_deref())?
            }
        }
        Commands::List(cmd) => {
            show_tsks_in_dir(
                cmd.openpgp_dir.as_deref(),
                cmd.filter,
                cmd.time.map(Into::into),
            )?;
        }
        Commands::Revoke(cmd) => {
            let cert = get_single_cert_from_dir(cmd.openpgp_dir.as_deref(), cmd.fingerprint)?;

            let cert = if cmd.all {
                revoke_subkey_of_cert(
                    cert.clone(),
                    cert.keys()
                        .subkeys()
                        .map(|subkey| subkey.fingerprint())
                        .collect(),
                    cmd.time.map(Into::into),
                    cmd.reason,
                    cmd.message.as_deref(),
                )?
            } else {
                revoke_subkey_of_cert(
                    cert,
                    cmd.subkey_fingerprint,
                    cmd.time.map(Into::into),
                    cmd.reason,
                    cmd.message.as_deref(),
                )?
            };

            if cmd.stdout {
                write_tsk_to_stdout(&cert)?
            } else {
                write_tsk(&cert, cmd.openpgp_dir.as_deref())?
            }
        }
        Commands::Proof(ProofCommand::Dns(DnsCommand::Add(cmd))) => {
            let cert = get_single_cert_from_dir(cmd.openpgp_dir.as_deref(), cmd.fingerprint)?;

            let cert = add_dns_proof(cert)?;

            if cmd.stdout {
                write_tsk_to_stdout(&cert)?
            } else {
                write_tsk(&cert, cmd.openpgp_dir.as_deref())?
            }
        }
    }
    Ok(())
}
