// SPDX-FileCopyrightText: 2023 David Runge <dave@sleepmap.de>
// SPDX-License-Identifier: Apache-2.0 OR MIT
use rstest::rstest;
use sshd_openpgp_auth::parse_known_hosts;
use testresult::TestResult;

mod common;
use common::ssh_known_hosts;
use common::Error;

#[cfg(feature = "online_tests")]
mod keyscan;

#[rstest]
fn test_parse_known_hosts(ssh_known_hosts: Result<String, Error>) -> TestResult {
    let ssh_public_keys = parse_known_hosts(&ssh_known_hosts?)?;
    println!("{:?}", ssh_public_keys);
    assert!(ssh_public_keys.len() == 3);
    Ok(())
}
